package com.iagoa.slsidwsc.rest.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.rest.dto.ProductDTO;

/**
 * @author <a href="iagoafe@ext.inditex.com">iagoafe</a>
 */
public class ProductDTOMapperImpl implements  ProductDTOMapper{
    /**
     * Convert to {@link ProductDTO} from {@link Product}
     * @param product the product
     * @return the product dto
     */
    @Override
    public ProductDTO toProductDTO(Product product) {
        if ( product == null ) {
            return null;
        }
        ProductDTO dto = new ProductDTO();
        dto.setId(product.getId());
        dto.setName(String.format("%s - %s", product.getName(), product.getProductType()));
        return dto;
    }

    /**
     * To list of {@link ProductDTO} form list of {@link Product}
     * @param products the products
     * @return the list
     */

    @Override
    public List<ProductDTO> toProductDTOs(List<Product> products){
        if ( products == null ) {
            return null;
        }
        return products
            .stream()
            .map(this::toProductDTO)
            .collect(Collectors.toList());
    }
}
