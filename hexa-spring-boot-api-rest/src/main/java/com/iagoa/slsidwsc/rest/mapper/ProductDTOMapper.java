package com.iagoa.slsidwsc.rest.mapper;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.rest.dto.ProductDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Product dto mapper.
 */
@Mapper
public interface ProductDTOMapper {

    ProductDTOMapper INSTANCE = Mappers.getMapper(ProductDTOMapper.class);

    /**
     * Convert to {@link ProductDTO} from {@link Product}
     * @param product the product
     * @return the product dto
     */
    ProductDTO toProductDTO(Product product);

    /**
     * To list of {@link ProductDTO} form list of {@link Product}
     * @param products the products
     * @return the list
     */
    List<ProductDTO> toProductDTOs(List<Product> products);

}
