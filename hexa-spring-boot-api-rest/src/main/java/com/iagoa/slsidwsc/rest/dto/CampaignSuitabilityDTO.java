package com.iagoa.slsidwsc.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Campaign Transfer Object
 */
@Data
@AllArgsConstructor(staticName = "of")
public class CampaignSuitabilityDTO {

    private Boolean suitable;

}
