package com.iagoa.slsidwsc.rest.dto;

import lombok.Data;

/**
 * Product Data Transfer Object
 */
@Data
public class ProductDTO {

    private Integer id;

    private String name;

}
