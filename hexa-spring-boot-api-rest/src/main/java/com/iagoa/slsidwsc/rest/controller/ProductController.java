package com.iagoa.slsidwsc.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.usecase.GetAllProductsByTypeUseCase;
import com.iagoa.slsidwsc.domain.usecase.ProductsBelongToCampaignUseCase;
import com.iagoa.slsidwsc.rest.dto.ProductDTO;
import com.iagoa.slsidwsc.rest.mapper.ProductDTOMapper;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Product Rest Controller
 */
@RestController
@RequestMapping(path = "/products")
public class ProductController {

    @Autowired
    private GetAllProductsByTypeUseCase getAllProductsByTypeUseCase;

    @Autowired
    private ProductsBelongToCampaignUseCase productsBelongToCampaignUseCase;

    private ProductDTOMapper mapper = ProductDTOMapper.INSTANCE;

    @GetMapping("/type/{type}")
    @ApiOperation(value = "Find all special products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProductDTO.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "InternalServerError")
    })
    public List<ProductDTO> getAllSpecialProducts(@PathVariable("type") String type) {
        return this.mapper.toProductDTOs(
                this.getAllProductsByTypeUseCase.getAllProductsByType(ProductType.valueOf(type.toUpperCase())));
    }

    @GetMapping("/campaign")
    @ApiOperation(value = "Determines products are suitable for campaign")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Boolean.class),
            @ApiResponse(code = 500, message = "InternalServerError")
    })
    public ResponseEntity<Object> areProductsSuitableForCampaign(@RequestParam("ids") final List<Integer> productIds) {
        return ResponseEntity.ok(this.productsBelongToCampaignUseCase.areProductsSuitableForCampaign(productIds));
    }

}
