package com.iagoa.slsidwsc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

/**
 * The Class Application.
 */

@SpringBootApplication
public class Application {

    /**
     * Metodo Main.
     * @param args argumentos de entrada
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
