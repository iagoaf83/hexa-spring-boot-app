package com.iagoa.slsidwsc.acceptance;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.iagoa.slsidwsc.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ProductAcceptanceIT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void whenEndpointQueried_thenRespondWithSpecialProducts() throws Exception {

        this.mvc.perform(get("/v1/products/special").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json("[" +
                    "  { " +
                    "    'id': 4, " +
                    "    'name': 'Product 4 - SPECIAL' " +
                    "  }, " +
                    "  { " +
                    "    'id': 5, " +
                    "    'name': 'Product 5 - SPECIAL' " +
                    "  }, " +
                    "  { " +
                    "    'id': 6, " +
                    "    'name': 'Product 6 - SPECIAL' " +
                    "  } " +
                    "]"));

    }

}
