package com.iagoa.slsidwsc.domain.entity;

/**
 * Product Type
 */
public enum ProductType {

    WEARABLE, COMPLEMENTS, ACCESSORIES, SPECIAL

}
