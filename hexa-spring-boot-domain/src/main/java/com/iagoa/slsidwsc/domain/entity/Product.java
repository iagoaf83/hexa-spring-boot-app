package com.iagoa.slsidwsc.domain.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product Domain Entity.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(access = AccessLevel.PUBLIC)
public class Product {

    /*
     * Important: Save metadata if your persistence model needs this information (see couchbase,
     * mongodb...)
     */
    // private Metadata metadata;

    private Integer id;

    private String name;

    private ProductType productType;

    public boolean isSpecial() {
        return this.productType.equals(ProductType.SPECIAL);
    }

}
