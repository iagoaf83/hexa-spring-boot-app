package com.iagoa.slsidwsc.domain.repository;

import java.util.Collection;
import java.util.List;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;

/**
 * Repository of products.
 */
public interface ProductRepository {

    /**
     * Find products by id
     * @param ids Ids of products to find
     * @return Same order list of product (a product is omitted from collections if not exists, never
     *         <code>null</code>)
     */
    List<Product> findProductsById(Collection<Integer> ids);

    /**
     * Find products of type.
     * @param productType the product type
     * @return the list of products of this type
     */
    List<Product> findProductsOfType(ProductType productType);

}
