package com.iagoa.slsidwsc.domain.usecase;

import java.util.List;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;

/**
 * Use case to get all products of special type.
 */
public interface GetAllProductsByTypeUseCase {

    /**
     * Gets all special products.
     * @return all special products
     */
    List<Product> getAllProductsByType(ProductType type);

}
