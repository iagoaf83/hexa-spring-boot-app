package com.iagoa.slsidwsc.domain.usecase;

import java.util.List;

import com.iagoa.slsidwsc.domain.entity.Product;

/**
 * <p>
 * Determinates if some group of products is suitable for campaign
 * </p>
 *
 * <p>
 * This campaign only applies if:
 * </p>
 *
 * <ul>
 * <li>Minimum number of product are 3.</li>
 * <li>All products are {@link Product#isSpecial() special}</li>
 * </ul>
 */
public interface ProductsBelongToCampaignUseCase {

    /**
     * Campaign suitability evaluator
     * @param productIds Ids of candidate products
     * @return <code>true</code> If products are suitable for campaing, <code>false</code> otherwise
     */
    boolean areProductsSuitableForCampaign(List<Integer> productIds);

}
