## [NOMBRE PROYECTO](https://axinic.central.inditex.grp/jira/browse/xxxxx)


## Introducción

Breve descripción funcional del proyecto.

### Documentación adicional

Enlaces a confluence.

### Proyectos relacionados

lista de proyectos.


## Quickstart

### Dependencias

listado de dependencias.

### Instalación

comandos para instalar aplicación.

### Ejecución

comandos para arrancar aplicación.

### API Quickstart

Ejemplos de uso del API.


## Tecnología

- Plataforma empleada (Java, Node, Go, etc.)
- Tipo de Proyecto:
  - Aplicación Web 
  - Servicio Web 
  - Aplicación de Escritorio 
  - Aplicación movil
  - Proceso Batch Java
- Versión del Framework utilizada
- Plataforma de despliegue (Openshift Intranet, WAS LC, etc.)


## Diseño Técnico

### Diagrama de Arquitectura

En este apartado se incluirá un diagrama de arquitectura de alto nivel que refleje la relación entre los artefactos del proyecto y otros sistemas relacionados. (por ejemplo Bases de Datos, Servicios, Colas, etc.)

### Estructura del proyecto

En este apartado se incluirá la relación de modulos del proyecto y la descricpión de su contenido.

### Integración con otros Sistemas

En este apartado se describirá cómo accede la aplicación a otros sistemas . Debe recogerse la integración con cualquier sistema tanto interno como externo.
