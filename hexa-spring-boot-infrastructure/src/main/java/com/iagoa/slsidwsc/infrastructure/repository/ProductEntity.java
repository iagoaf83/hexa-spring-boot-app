package com.iagoa.slsidwsc.infrastructure.repository;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Storage product entity form
 */
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ProductEntity {

    /*
     * Important: Save metadata if your persistence model needs this information (see couchbase,
     * mongodb...)
     */
    //private Metadata metadata;

    @Id
    private Integer id;

    @Field
    private String name;

    @Field
    private String type;

}
