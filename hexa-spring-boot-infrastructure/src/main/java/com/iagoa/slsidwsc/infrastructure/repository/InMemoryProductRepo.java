package com.iagoa.slsidwsc.infrastructure.repository;


import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;

/**
 * <p>
 * Implementation of {@link ProductRepository} with particular storage details
 * </p>
 *
 * <p>
 * NOTE: In this naive example, storage will be memory
 * </p>
 */
@Repository
public class InMemoryProductRepo{

    private Map<Integer, ProductEntity> inMemoryData = new HashMap<>();

    public InMemoryProductRepo() {
        this.populateInMemoryData();
    }

    private void populateInMemoryData() {

        ProductEntity product = new ProductEntity();
        product.setId(1);
        product.setName("Product 1");
        product.setType(ProductType.WEARABLE.name());
        this.inMemoryData.put(1, product);

        product = new ProductEntity();
        product.setId(2);
        product.setName("Product 2");
        product.setType(ProductType.COMPLEMENTS.name());
        this.inMemoryData.put(2, product);

        product = new ProductEntity();
        product.setId(3);
        product.setName("Product 3");
        product.setType(ProductType.ACCESSORIES.name());
        this.inMemoryData.put(3, product);

        product = new ProductEntity();
        product.setId(4);
        product.setName("Product 4");
        product.setType(ProductType.SPECIAL.name());
        this.inMemoryData.put(4, product);

        product = new ProductEntity();
        product.setId(5);
        product.setName("Product 5");
        product.setType(ProductType.SPECIAL.name());
        this.inMemoryData.put(5, product);

        product = new ProductEntity();
        product.setId(6);
        product.setName("Product 6");
        product.setType(ProductType.SPECIAL.name());
        this.inMemoryData.put(6, product);

    }

    public List<ProductEntity> findById(final Collection<Integer> ids) {
        return this.inMemoryData.values()
            .stream()
            .filter(product -> ids.contains(product.getId()))
            .collect(Collectors.toList());
    }

    public List<ProductEntity> findByType(final String productType) {
        return this.inMemoryData.values()
            .stream()
            .filter(t -> t.getType().equals(productType))
            .collect(Collectors.toList());
    }

}

