package com.iagoa.slsidwsc.infrastructure.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * Mapper for {@link ProductEntity}.
 */

@Mapper
public interface ProductEntityMapper {

    ProductEntityMapper INSTANCE = Mappers.getMapper(ProductEntityMapper.class);

    public Product toDomainEntity(ProductEntity storageProduct);

    /**
     * From entity to domain product
     *
     * @param domainProduct Product in the domain from
     * @return the storage product entity
     */

    ProductEntity fromDomainEntity(Product domainProduct);
}
