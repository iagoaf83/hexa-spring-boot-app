package com.iagoa.slsidwsc.infrastructure.repository;

import org.springframework.stereotype.Component;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;

/**
 * @author <a href="iagoafe@ext.inditex.com">iagoafe</a>
 */

@Component
public class ProductEntityMapperImpl implements ProductEntityMapper{
    @Override
    public Product toDomainEntity(ProductEntity storageProduct){
        if ( storageProduct == null ) {
            return null;
        }

        Product product = new Product();

        product.setId(storageProduct.getId());
        product.setName(storageProduct.getName());
        product.setProductType(ProductType.valueOf(storageProduct.getType()));

        return product;
    }

    /**
     * From entity to domain product
     * @param domainProduct Product in the domain from
     * @return the storage product entity
     */

    @Override
    public ProductEntity fromDomainEntity(Product domainProduct){
        if ( domainProduct == null ) {
            return null;
        }

        ProductEntity product = new ProductEntity();

        product.setId(domainProduct.getId());
        product.setName(domainProduct.getName());
        product.setType(domainProduct.getProductType().name());

        return product;
    }
}
