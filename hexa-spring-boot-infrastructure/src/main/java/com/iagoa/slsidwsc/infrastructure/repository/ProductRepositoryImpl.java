package com.iagoa.slsidwsc.infrastructure.repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;

/**
 * <p>
 * Implementation of {@link ProductRepository} with particular storage details
 * </p>
 *
 * <p>
 * NOTE: In this naive example, storage will be memory
 * </p>
 */

@Repository
public class ProductRepositoryImpl implements ProductRepository {

   // @Autowired
   // private CouchBaseProductRepo couchBaseProdRepo;

    @Autowired
    private InMemoryProductRepo inMemProdRepo;

    private ProductEntityMapper mapper = ProductEntityMapper.INSTANCE;

    @Override
    public List<Product> findProductsById(final Collection<Integer> ids) {
        String strategy = chooseStrategy();
        List<ProductEntity> products;
        switch(strategy){
            case "in_memory":
                products = inMemProdRepo.findById(ids);
                break;
            /*case "couchbase":
                products = couchBaseProdRepo.findAllById(ids);
                break;*/
            default:
                products = inMemProdRepo.findById(ids);
                break;
        }

        return products
            .stream()
            .map(this.mapper::toDomainEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<Product> findProductsOfType(final ProductType productType) {
        String strategy = chooseStrategy();
        List<ProductEntity> products;
        switch(strategy){
            case "in_memory":
                products = inMemProdRepo.findByType(productType.name());
                break;
            /*case "couchbase":
                products = couchBaseProdRepo.findByType(productType.name());
                break;*/
            default:
                products = inMemProdRepo.findByType(productType.name());
                break;
        }

        return products
            .stream()
            .map(this.mapper::toDomainEntity)
            .collect(Collectors.toList());
    }

    private String chooseStrategy(){
        return "in_memory";
    }

}
