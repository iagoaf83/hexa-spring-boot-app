package com.iagoa.slsidwsc.usecase;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;
import com.iagoa.slsidwsc.domain.usecase.ProductsBelongToCampaignUseCase;

/**
 * Implementation of use case {@link ProductsBelongToCampaignUseCase}
 */
@Component
public class ProductsBelongToCampaignUseCaseImpl implements ProductsBelongToCampaignUseCase {

    @Autowired
    private ProductRepository productRepository;

    /**
     * Instantiates a new "Products campaign use case" given a particular {@link ProductRepository}
     * @param productRepository the product repository
     */
    public ProductsBelongToCampaignUseCaseImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public boolean areProductsSuitableForCampaign(final List<Integer> productIds) {
        final List<Product> candidates = this.productRepository.findProductsById(productIds);
        return this.productsMatchCampaign(candidates);
    }

    private boolean productsMatchCampaign(final List<Product> candidates) {
        // Business logic here
        final boolean moreThanThree = candidates.size() >= 3;
        final boolean areAllSpecial = candidates.stream().allMatch(Product::isSpecial);
        return moreThanThree && areAllSpecial;
    }

}
