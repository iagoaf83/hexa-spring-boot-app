package com.iagoa.slsidwsc.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;
import com.iagoa.slsidwsc.domain.usecase.GetAllProductsByTypeUseCase;

/**
 * Implementation of use case {@link GetAllProductsByTypeUseCase}
 */
@Component
public class GetAllProductsByTypeUseCaseImpl implements GetAllProductsByTypeUseCase {

    @Autowired
    private ProductRepository productRepository;

    /**
     * Instantiates a new "Get all special products use case" given a particular
     * {@link ProductRepository}
     * @param productRepository the product repository
     */
    public GetAllProductsByTypeUseCaseImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProductsByType(ProductType type) {
        // Business logic here
        return this.productRepository.findProductsOfType(type);
    }

}
