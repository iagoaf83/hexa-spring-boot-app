package com.iagoa.slsidwsc.usecase.fixtures;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;

/**
 * Provides products fixtures just for test purpose
 */
public class ProductTestDataBuilder {

    private static List<Product> productSet;

    static {
        productSet = Arrays.asList(
                Product.builder()
                    .id(1)
                    .name("Product 1")
                    .productType(ProductType.WEARABLE)
                    .build(),
                Product.builder()
                    .id(2)
                    .name("Product 2")
                    .productType(ProductType.COMPLEMENTS)
                    .build(),
                Product.builder()
                    .id(3)
                    .name("Product 3")
                    .productType(ProductType.ACCESSORIES)
                    .build(),
                Product.builder()
                    .id(4)
                    .name("Product 4")
                    .productType(ProductType.SPECIAL)
                    .build(),
                Product.builder()
                    .id(5)
                    .name("Product 5")
                    .productType(ProductType.SPECIAL)
                    .build(),
                Product.builder()
                    .id(6)
                    .name("Product 6")
                    .productType(ProductType.SPECIAL)
                    .build());
    }

    public static List<Product> noProducts() {
        return Collections.emptyList();
    }

    public static List<Product> allProducts() {
        return Collections.unmodifiableList(productSet);
    }

    public static List<Product> onlyWearable() {
        return productSet.stream()
            .filter(product -> product.getProductType().equals(ProductType.WEARABLE))
            .collect(Collectors.toList());
    }

    public static List<Integer> idsOfTwoProductsOfSpecialType() {
        return Arrays.asList(4, 5);
    }

    public static List<Integer> idsOfThreeSpecialProductsAndOneOfWearableType() {
        return Arrays.asList(1, 4, 5, 6);
    }

    public static List<Integer> idsOfThreeProductsOfSpecialType() {
        return Arrays.asList(4, 5, 6);
    }

}
