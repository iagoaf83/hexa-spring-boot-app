package com.iagoa.slsidwsc.usecase;

import java.util.List;

import com.iagoa.slsidwsc.domain.repository.ProductRepository;
import com.iagoa.slsidwsc.domain.usecase.ProductsBelongToCampaignUseCase;
import com.iagoa.slsidwsc.usecase.fixtures.TestDoubleProductRepository;
import com.iagoa.slsidwsc.usecase.fixtures.ProductTestDataBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests {@link ProductsBelongToCampaignUseCaseImpl} behaviour providing a test implementation of
 * {@link ProductRepository} called {@link TestDoubleProductRepository} just for testing.
 */
@RunWith(JUnit4.class)
public class ProductsBelongToCampaignUseCaseTest {

    private ProductsBelongToCampaignUseCase productsBelongToCampaignUseCase;

    @Before
    public void setUpUseCase() {
        this.productsBelongToCampaignUseCase = new ProductsBelongToCampaignUseCaseImpl(TestDoubleProductRepository
            .of(ProductTestDataBuilder.allProducts()));
    }

    @Test
    public void givenLessThanThreeProducts_thenCampaignIsNotSuitable() {

        // Given
        final List<Integer> twoSpecialProducts = ProductTestDataBuilder.idsOfTwoProductsOfSpecialType();

        // When
        final boolean areSuitable = this.productsBelongToCampaignUseCase
            .areProductsSuitableForCampaign(twoSpecialProducts);

        // Then
        assertThat(areSuitable).isFalse();

    }

    @Test
    public void givenEnoughProductsAndAllSpecial_thenCampaignIsSuitable() {

        // Given
        final List<Integer> threeProductsSpecialProducts = ProductTestDataBuilder.idsOfThreeProductsOfSpecialType();

        // When
        final boolean areSuitable = this.productsBelongToCampaignUseCase
            .areProductsSuitableForCampaign(threeProductsSpecialProducts);

        // Then
        assertThat(areSuitable).isTrue();

    }

    @Test
    public void givenEnoughProductsButSomeAreNotSpecial_thenCampaignIsNotSuitable() {

        // Given
        final List<Integer> threeProductsOfMixedType = ProductTestDataBuilder
            .idsOfThreeSpecialProductsAndOneOfWearableType();

        // When
        final boolean areSuitable = this.productsBelongToCampaignUseCase
            .areProductsSuitableForCampaign(threeProductsOfMixedType);

        // Then
        assertThat(areSuitable).isFalse();

    }

}
