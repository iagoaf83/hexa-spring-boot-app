package com.iagoa.slsidwsc.usecase;

import static com.iagoa.slsidwsc.usecase.fixtures.ProductTestDataBuilder.allProducts;
import static com.iagoa.slsidwsc.usecase.fixtures.ProductTestDataBuilder.noProducts;
import static com.iagoa.slsidwsc.usecase.fixtures.ProductTestDataBuilder.onlyWearable;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;
import com.iagoa.slsidwsc.domain.usecase.GetAllProductsByTypeUseCase;
import com.iagoa.slsidwsc.usecase.fixtures.TestDoubleProductRepository;

/**
 * Tests {@link GetAllProductsByTypeUseCaseImpl} behaviour providing a test implementation of
 * {@link ProductRepository} called {@link TestDoubleProductRepository} just for testing.
 */
@RunWith(JUnit4.class)
public class GetAllSpecialProductsUseCaseTest {

    @Test
    public void givenNoProductsRepository_thenResultIsEmpty() {

        // Given
        final GetAllProductsByTypeUseCase productsCampaignUseCase = new GetAllProductsByTypeUseCaseImpl(
                TestDoubleProductRepository.of(noProducts()));

        // When
        final List<Product> specialProducts = productsCampaignUseCase.getAllProductsByType(ProductType.SPECIAL);

        // Then
        assertThat(specialProducts).hasSize(0);

    }

    @Test
    public void givenOnlyWearableRepository_thenResultIsEmpty() {

        // Given
        final GetAllProductsByTypeUseCase productsCampaignUseCase = new GetAllProductsByTypeUseCaseImpl(
                TestDoubleProductRepository.of(onlyWearable()));

        // When
        final List<Product> specialProducts = productsCampaignUseCase.getAllProductsByType(ProductType.SPECIAL);

        // Then
        assertThat(specialProducts).hasSize(0);

    }

    @Test
    public void givenMixedProductsTyped_thenResultHasOnlySpecial() {

        // Given
        final GetAllProductsByTypeUseCase productsCampaignUseCase = new GetAllProductsByTypeUseCaseImpl(
                TestDoubleProductRepository.of(allProducts()));

        // When
        final List<Product> specialProducts = productsCampaignUseCase.getAllProductsByType(ProductType.SPECIAL);

        // Then
        assertThat(specialProducts)
        .hasSize(3)
        .allMatch(Product::isSpecial);

    }

}
