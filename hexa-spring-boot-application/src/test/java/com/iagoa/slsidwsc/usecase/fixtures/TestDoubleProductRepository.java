package com.iagoa.slsidwsc.usecase.fixtures;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import com.iagoa.slsidwsc.domain.entity.Product;
import com.iagoa.slsidwsc.domain.entity.ProductType;
import com.iagoa.slsidwsc.domain.repository.ProductRepository;
import lombok.AllArgsConstructor;

/**
 * In memory repository of {@link ProductRepository} just for test purposes
 */
@AllArgsConstructor(staticName = "of")
public class TestDoubleProductRepository implements ProductRepository {

    @NotNull
    private List<Product> inMemoryData;

    @Override
    public List<Product> findProductsById(final Collection<Integer> ids) {
        return this.inMemoryData.stream()
            .filter(product -> ids.contains(product.getId()))
            .collect(Collectors.toList());
    }

    @Override
    public List<Product> findProductsOfType(final ProductType productType) {
        return this.inMemoryData.stream()
            .filter(t -> t.getProductType().equals(productType))
            .collect(Collectors.toList());
    }

}
